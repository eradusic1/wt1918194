const app = require("../index.js");
const chai = require("chai");
const chaiHttp = require("chai-http");
const { expect } = chai;
chai.use(chaiHttp);

before((done) => { app.on('Omoguceno testiranje', () => { done(); }) });

describe("OSOBLJE", () => {
    it("Pokupio tri osobe iz baze", done => {
        chai
            .request(app)
            .get("/osoblje")
            .end((err, res) => {
                let sadrzaj = res.body;
                expect(sadrzaj['imena']).to.have.length(3);
                done();
            })
    });
    it("Pokupio sva tačna imena osoba iz baze", done => {
        chai
            .request(app)
            .get("/osoblje")
            .end((err, res) => {
                let sadrzaj = res.body;
                expect(sadrzaj['imena']).to.contain("Test Test");
                expect(sadrzaj['imena']).to.contain("Neko Nekic");
                expect(sadrzaj['imena']).to.contain("Drugi Neko");
                done();
            })
    });
});

describe("SALE", () => {
    it("Pokupio dvije sale iz baze", done => {
        chai
            .request(app)
            .get("/sale")
            .end((err, res) => {
                let sadrzaj = res.body;
                expect(sadrzaj['nazivi']).to.have.length(2);
                done();
            })
    });
    it("Pokupio tačne nazive sala iz baze", done => {
        chai
            .request(app)
            .get("/sale")
            .end((err, res) => {
                let sadrzaj = res.body;
                expect(sadrzaj['nazivi']).to.contain("1-11");
                expect(sadrzaj['nazivi']).to.contain("1-15");
                done();
            })
    });
});

describe("ZAUZECA", () => {
    it("Pokupio dva zauzeca iz baze, jedno periodicno, jedno vanredno", done => {
        chai
            .request(app)
            .get("/zauzeca")
            .end((err, res) => {
                let sadrzaj = res.body;
                expect(sadrzaj['periodicna']).to.have.length(1);
                expect(sadrzaj['vanredna']).to.have.length(1);
                done();
            })
    });

    it("Postojanje novododanog periodicnog zauzeca u listi zauzeca", done => {
        chai
            .request(app)
            .post("/")
            .send({ zauzece: { dan: 4, semestar: "zimski", pocetak: "09:00", kraj: "21:00", naziv: "1-11", predavac: "Neko Nekic" }, jeLiPeriodicno: true, datum: '', })
            .end((err, res) => {
                let postoji = false;
                let periodicne = res.body['periodicna'];
                for (let i = 0; i < periodicne.length; i++) {
                    if (periodicne[i]['dan'] == 4 && periodicne[i]['semestar'] == 'zimski' && periodicne[i]['pocetak'] == '09:00' && periodicne[i]['kraj'] == '21:00' && periodicne[i]['naziv'] == '1-11' && periodicne[i]['predavac'] == 'Neko Nekic') {
                        postoji = true;
                        break;
                    }
                }
                expect(postoji).equals(true);
                expect(periodicne).to.have.length(2);
                done();
            })
    });

    it("Pokusaj dodavanja nevalidne vanredne rezervacije - broj zauzeca se nije promijenio, postoji samo ono iz baze", done => {
        chai
            .request(app)
            .post("/")
            .send({ zauzece: {datum:"01.01.2020",pocetak:"12:00",kraj:"13:00",naziv:"1-11",predavac:"Drugi Neko"}, jeLiPeriodicno: false, datum: '', })
            .end((err, res) => {
                let sadrzaj = res.body;
                expect(sadrzaj['imaAlerta']).equals(true);
                let vanredne = sadrzaj['vanredna'];
                expect(vanredne).to.have.length(1);
                done();
            })
    });

});

describe("REZERVACIJE", () => {
    it("Dodao validnu redovnu rezervaciju", done => {
        chai
            .request(app)
            .post("/")
            .send({ zauzece: { dan: 3, semestar: "zimski", pocetak: "09:00", kraj: "21:00", naziv: "1-11", predavac: "Neko Nekic" }, jeLiPeriodicno: true, datum: '', })
            .end((err, res) => {
                let sadrzaj = res.body;
                expect(sadrzaj['imaAlerta']).equals(false);
                let postoji = false;
                let periodicne = sadrzaj['periodicna'];
                for (let i = 0; i < periodicne.length; i++) {
                    if (periodicne[i]['dan'] == 3 && periodicne[i]['semestar'] == 'zimski' && periodicne[i]['pocetak'] == '09:00' && periodicne[i]['kraj'] == '21:00' && periodicne[i]['naziv'] == '1-11' && periodicne[i]['predavac'] == 'Neko Nekic') {
                        postoji = true;
                        break;
                    }
                }
                expect(postoji).equals(true);
                done();
            })
    });

    it("Dodao validnu vanrednu rezervaciju", done => {
        chai
            .request(app)
            .post("/")
            .send({ zauzece: {datum:"23.01.2020",pocetak:"15:00",kraj:"17:00",naziv:"1-15",predavac:"Drugi Neko"}, jeLiPeriodicno: false, datum: '', })
            .end((err, res) => {
                let sadrzaj = res.body;
                expect(sadrzaj['imaAlerta']).equals(false);
                let postoji = false;
                let vanredne = sadrzaj['vanredna'];
                for (let i = 0; i < vanredne.length; i++) {
                    if (vanredne[i]['datum'] == '23.01.2020'  && vanredne[i]['pocetak'] == '15:00' && vanredne[i]['kraj'] == '17:00' && vanredne[i]['naziv'] == '1-15' && vanredne[i]['predavac'] == 'Drugi Neko') {
                        postoji = true;
                        break;
                    }
                }
                expect(postoji).equals(true);
                done();
            })
    });

    it("Pokusaj dodavanja nevalidne redovne rezervacije - alert", done => {
        chai
            .request(app)
            .post("/")
            .send({ zauzece: { dan: 2, semestar: "zimski", pocetak: "09:00", kraj: "21:00", naziv: "1-11", predavac: "Neko Nekic" }, jeLiPeriodicno: true, datum: '', })
            .end((err, res) => {
                let sadrzaj = res.body;
                expect(sadrzaj['imaAlerta']).equals(true);
                let postoji = false;
                let periodicne = sadrzaj['periodicna'];
                for (let i = 0; i < periodicne.length; i++) {
                    if (periodicne[i]['dan'] == 2 && periodicne[i]['semestar'] == 'zimski' && periodicne[i]['pocetak'] == '09:00' && periodicne[i]['kraj'] == '21:00' && periodicne[i]['naziv'] == '1-11' && periodicne[i]['predavac'] == 'Neko Nekic') {
                        postoji = true;
                        break;
                    }
                }
                expect(postoji).equals(false);
                expect(sadrzaj['alert']).to.contain('Salu je rezervisao uposlenik');
                done();
            })
    });

});