const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const fs = require('fs');
const db = require('./db.js');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

let zimski = [9, 10, 11, 0];
let ljetni = [1, 2, 3, 4, 5];

db.sequelize.sync({ force: true }).then(() => {
    db.osoblje.create({ ime: 'Neko', prezime: 'Nekic', uloga: 'profesor' }
    ).then(() => {
        db.osoblje.create({ ime: 'Drugi', prezime: 'Neko', uloga: 'asistent' }
        ).then(() => {
            db.osoblje.create({ ime: 'Test', prezime: 'Test', uloga: 'asistent' }
            ).then(() => {
                db.sala.create({ naziv: '1-11', zaduzenaOsoba: 1 }
                ).then(() => {
                    db.sala.create({ naziv: '1-15', zaduzenaOsoba: 2 }
                    ).then(() => {
                        db.termin.create({ redovni: false, datum: '01.01.2020', pocetak: '12:00', kraj: '13:00' }
                        ).then(() => {
                            db.termin.create({ redovni: true, dan: 0, semestar: 'zimski', pocetak: '13:00', kraj: '14:00' }
                            ).then(() => {
                                db.rezervacija.create({ termin: 1, sala: 1, osoba: 1 }
                                ).then(() => {
                                    db.rezervacija.create({ termin: 2, sala: 1, osoba: 3 }
                                    ).then(() => {
                                        app.listen(8080);
                                        app.emit('Omoguceno testiranje');
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
});


app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/pocetna.html');
});

app.post('/', (req, res) => {
    let objekt = req.body;
    let zauzece = objekt['zauzece'];
    let periodicno = objekt['jeLiPeriodicno'];
    let datumStringZaPoruku = objekt['datum'];
    let periodicnaZuzeca = [];
    let vanrednaZauzeca = [];
    db.rezervacija.findAll().then((results) => {
        for (let i = 0; i < results.length; i++) {
            db.sala.findOne({ where: { id: results[i]['sala'] } }).then((sala) => {
                db.osoblje.findOne({ where: { id: results[i]['osoba'] } }).then((osoba) => {
                    db.termin.findOne({ where: { id: results[i]['termin'] } }).then((termin) => {
                        let jeLiPeriodicno = termin['redovni'];
                        if (jeLiPeriodicno) {
                            periodicnaZuzeca.push(RedovnoZauzece(termin['dan'], termin['semestar'], termin['pocetak'], termin['kraj'], sala['naziv'], osoba['ime'] + ' ' + osoba['prezime']));
                        }
                        else {
                            vanrednaZauzeca.push(VanrednoZauzece(termin['datum'], termin['pocetak'], termin['kraj'], sala['naziv'], osoba['ime'] + ' ' + osoba['prezime']));
                        }
                        if (i == results.length - 1) {
                            let posaljiAlert = false;
                            let novo = false;
                            let stringAlerta = "Nije moguće rezervisati salu " + zauzece['naziv'] + " za navedeni datum " + datumStringZaPoruku + " i termin od " + zauzece['pocetak'] + " do " + zauzece['kraj'] + "!\nSalu je rezervisao uposlenik: " + zauzece['predavac'];
                            if (periodicno) {
                                let postoji1 = postojiUPeriodicnim(zauzece, periodicnaZuzeca, true);
                                let postoji2 = postojiUVanrednim(zauzece, vanrednaZauzeca, true);
                                if (postoji1 || postoji2) {
                                    posaljiAlert = true;
                                }
                                else { periodicnaZuzeca.push(zauzece); novo = true; }
                            } else {
                                let postoji1 = postojiUPeriodicnim(zauzece, periodicnaZuzeca, false);
                                let postoji2 = postojiUVanrednim(zauzece, vanrednaZauzeca, false);
                                if (postoji1 || postoji2) {
                                    posaljiAlert = true;
                                }
                                else { vanrednaZauzeca.push(zauzece); novo = true; }
                            }
                            if (novo) {
                                if (periodicno) {
                                    db.termin.create({ redovni: true, dan: zauzece['dan'], semestar: zauzece['semestar'], pocetak: zauzece['pocetak'], kraj: zauzece['kraj'] }).then((novi) => {
                                        db.sala.findOne({ where: { naziv: zauzece['naziv'] } }).then((sala) => {
                                            osoba = zauzece['predavac'].split(' ');
                                            db.osoblje.findOne({ where: { ime: osoba[0], prezime: osoba[1] } }).then((predavac) => {
                                                db.rezervacija.create({ osoba: predavac['id'], termin: novi['id'], sala: sala['id'] });
                                                let noviObjekat = {};
                                                if (posaljiAlert) noviObjekat = { periodicna: periodicnaZuzeca, vanredna: vanrednaZauzeca, imaAlerta: true, alert: '"' + stringAlerta + '"' };
                                                else noviObjekat = { periodicna: periodicnaZuzeca, vanredna: vanrednaZauzeca, imaAlerta: false, alert: '"' + stringAlerta + '"' };
                                                res.json(noviObjekat);
                                            });
                                        });
                                    });
                                } else {
                                    db.termin.create({ redovni: false, datum: zauzece['datum'], pocetak: zauzece['pocetak'], kraj: zauzece['kraj'] }).then((novi) => {
                                        db.sala.findOne({ where: { naziv: zauzece['naziv'] } }).then((sala) => {
                                            osoba = zauzece['predavac'].split(' ');
                                            db.osoblje.findOne({ where: { ime: osoba[0], prezime: osoba[1] } }).then((predavac) => {
                                                db.rezervacija.create({ osoba: predavac['id'], termin: novi['id'], sala: sala['id'] });
                                                let noviObjekat = {};
                                                if (posaljiAlert) noviObjekat = { periodicna: periodicnaZuzeca, vanredna: vanrednaZauzeca, imaAlerta: true, alert: '"' + stringAlerta + '"' };
                                                else noviObjekat = { periodicna: periodicnaZuzeca, vanredna: vanrednaZauzeca, imaAlerta: false, alert: '"' + stringAlerta + '"' };
                                                res.json(noviObjekat);
                                            });
                                        });
                                    });
                                }

                            } else {
                                let noviObjekat = {};
                                if (posaljiAlert) noviObjekat = { periodicna: periodicnaZuzeca, vanredna: vanrednaZauzeca, imaAlerta: true, alert: '"' + stringAlerta + '"' };
                                else noviObjekat = { periodicna: periodicnaZuzeca, vanredna: vanrednaZauzeca, imaAlerta: false, alert: '"' + stringAlerta + '"' };
                                res.json(noviObjekat);
                            }
                        }
                    });
                });
            });
        }
    });
});

app.get('/zauzeca', (req, res) => {
    let redovna = [];
    let vanredna = [];
    db.rezervacija.findAll().then((results) => {
        for (let i = 0; i < results.length; i++) {
            db.sala.findOne({ where: { id: results[i]['sala'] } }).then((sala) => {
                db.osoblje.findOne({ where: { id: results[i]['osoba'] } }).then((osoba) => {
                    db.termin.findOne({ where: { id: results[i]['termin'] } }).then((termin) => {
                        let jeLiPeriodicno = termin['redovni'];
                        if (jeLiPeriodicno) {
                            redovna.push(RedovnoZauzece(termin['dan'], termin['semestar'], termin['pocetak'], termin['kraj'], sala['naziv'], osoba['ime'] + ' ' + osoba['prezime']));
                        }
                        else {
                            vanredna.push(VanrednoZauzece(termin['datum'], termin['pocetak'], termin['kraj'], sala['naziv'], osoba['ime'] + ' ' + osoba['prezime']));
                        }
                        if (i == results.length - 1) {
                            res.json({ periodicna: redovna, vanredna: vanredna });
                        }
                    });
                });
            });
        }
    });
});

//app.listen(8080);

function postojiUVanrednim(rezervacija, lista, periodicnaRezervacija) {
    let i;
    for (i = 0; i < lista.length; i++) {
        let ime = (rezervacija['naziv'] == lista[i]['naziv']);
        let termin = terminZauzet(rezervacija, lista[i]['pocetak'], lista[i]['kraj']);
        if (periodicnaRezervacija) {
            let d = dajDatum(lista[i]['datum']);
            let postojeciDan = d.getDay();
            if (postojeciDan == 0) postojeciDan = 7;
            postojeciDan--;
            let postojeciMjesec = d.getMonth();
            let dan = (postojeciDan == rezervacija['dan']);
            let mjesec = false;
            if (rezervacija['semestar'] == 'zimski' && zimski.includes(postojeciMjesec)) mjesec = true;
            else if (rezervacija['semestar'] == 'ljetni' && ljetni.includes(postojeciMjesec)) mjesec = true;
            if (ime && dan && mjesec && termin) return true;
        }
        else {
            if (ime && termin && rezervacija['datum'] == lista[i]['datum']) return true;
        }
    }
    return false;
}

function postojiUPeriodicnim(rezervacija, lista, periodicnaRezervacija) {
    let i;
    for (i = 0; i < lista.length; i++) {
        let ime = (rezervacija['naziv'] == lista[i]['naziv']);
        let termin = terminZauzet(rezervacija, lista[i]['pocetak'], lista[i]['kraj']);
        if (!periodicnaRezervacija) {
            let d = dajDatum(rezervacija['datum']);
            let postojeciDan = d.getDay();
            if (postojeciDan == 0) postojeciDan = 7;
            postojeciDan--;
            let postojeciMjesec = d.getMonth();
            let dan = (postojeciDan == lista[i]['dan']);
            let mjesec = false;
            if (lista[i]['semestar'] == 'zimski' && zimski.includes(postojeciMjesec)) mjesec = true;
            else if (rezervacija['semestar'] == 'ljetni' && ljetni.includes(postojeciMjesec)) mjesec = true;
            if (ime && dan && mjesec && termin) return true;
        }
        else {
            if (ime && termin && rezervacija['dan'] == lista[i]['dan'] && rezervacija['semestar'] == lista[i]['semestar']) return true;
        }
    }
    return false;
}

function terminZauzet(zauzece, pocetak, kraj) {
    if (pocetak == kraj) return false;
    if (pocetak > kraj) return false;

    let pocetakSala = new Date('2/2/2000 ' + zauzece.pocetak);
    let krajSala = new Date('2/2/2000 ' + zauzece.kraj);
    let pocetakOgranicenje = new Date('2/2/2000 ' + pocetak);
    let krajOgranicenje = new Date('2/2/2000 ' + kraj);

    if (pocetakSala >= pocetakOgranicenje && pocetakSala < krajOgranicenje) return true;
    if (krajSala > pocetakOgranicenje && krajSala <= krajOgranicenje) return true;
    if (pocetakOgranicenje >= pocetakSala && pocetakOgranicenje < krajSala) return true;
    if (krajOgranicenje > pocetakSala && krajOgranicenje <= krajSala) return true;

    return false;
}

function dajDatum(datum1) {
    var noviDatum = datum1.split(".");
    noviDatum.reverse();
    noviDatum.join("-");
    return new Date(noviDatum);
}

app.post('/srcSlike', (req, res) => {
    const testFolder = 'public/slike';
    const fs = require('fs');
    let nizNaziva = [];
    let objekt = req.body;
    let ucitane = objekt['ucitaneSlike'];
    fs.readdir(testFolder, (err, files) => {
        files.forEach(file => {
            nizNaziva.push(file);
        });
        let i;
        let zaSlanje = ['', '', ''];
        let brojac = 0;
        if (nizNaziva.length > ucitane.length)
            for (i = 0; i < nizNaziva.length; i++) {
                if (!ucitane.includes(nizNaziva[i])) {
                    brojac++;
                    zaSlanje[brojac - 1] = nizNaziva[i];
                    if (brojac == 3) break;
                }
            }
        let kraj = (nizNaziva.length == ucitane.length + brojac);
        let sources = { src1: zaSlanje[0], src2: zaSlanje[1], src3: zaSlanje[2], jeLiKraj: kraj };
        res.json(sources);
        res.end();
    });
});

app.get('/osoblje', (req, res) => {
    db.osoblje.findAll().then((results) => {
        let listaOsoba = [];
        for (i = 0; i < results.length; i++) {
            listaOsoba.push(results[i]['ime'] + ' ' + results[i]['prezime']);
        }
        res.json({ imena: listaOsoba });
        res.end();
    });
});

app.get('/sale', (req, res) => {
    db.sala.findAll().then((results) => {
        let listaNaziva = [];
        for (i = 0; i < results.length; i++) {
            listaNaziva.push(results[i]['naziv']);
        }
        res.json({ nazivi: listaNaziva });
        res.end();
    });
});

app.get('/lokacije', (req, res) => {
    let osobe = [];
    db.osoblje.findAll().then((results) => {
        for (let i = 0; i < results.length; i++) {
            db.rezervacija.findAll({ where: { osoba: results[i]['id'] } }).then((rezervacije) => {
                if (rezervacije.length == 0) {
                    osobe.push(results[i]['ime'] + ' ' + results[i]['prezime'] + ', ' + results[i]['uloga'] + ' - u kancelariji');
                    if (i == results.length - 1) {
                        console.log("izlaz1");
                        res.send({ osobe: osobe });
                        res.end();
                    }
                }
                for (let k = 0; k < rezervacije.length; k++) {
                    let rezervacija = rezervacije[k];
                    db.termin.findOne({ where: { id: rezervacija['termin'] } }).then((termin) => {
                        db.sala.findOne({ where: { id: rezervacija['sala'] } }).then((sala) => {
                            if (terminSada(termin['pocetak'], termin['kraj']) && danasnjiDan(termin)) {
                                osobe.push(results[i]['ime'] + ' ' + results[i]['prezime'] + ', ' + results[i]['uloga'] + ' - u sali ' + sala['naziv']);
                            }
                            if (k == rezervacije.length - 1) {
                                let nadjena = false;
                                for (let n = 0; n < osobe.length; n++) if (osobe[n].includes(results[i]['ime'] + ' ' + results[i]['prezime'], 0)) nadjena = true;
                                if (!nadjena) osobe.push(results[i]['ime'] + ' ' + results[i]['prezime'] + ', ' + results[i]['uloga'] + ' - u kancelariji');
                            }
                            if (i == results.length - 1 && k == rezervacije.length - 1) {
                                res.send({ osobe: osobe });
                                res.end();
                            }
                        });
                    });
                }
            });

        }
    });
});

function terminSada(pocetak, kraj) {
    let pocetakTermina = pocetak.split(':');
    let krajTermina = kraj.split(':');
    let p = new Date();
    p.setHours(pocetakTermina[0]);
    p.setMinutes(pocetakTermina[1]);
    p.setSeconds(0);

    let k = new Date();
    k.setHours(krajTermina[0]);
    k.setMinutes(krajTermina[1]);
    k.setSeconds(0);

    var here = new Date();
    return (here.getTime() >= p.getTime() && here.getTime() <= k.getTime());
}

function danasnjiDan(termin) {
    if (termin['redovni']) {
        let dan = termin['dan'];
        let semestar = termin['semestar'];
        let danas = new Date();
        let danUSedmici = danas.getDay();
        if (danUSedmici == 0) danUSedmici = 6;
        else danUSedmici--;
        let istiSemestar = false;
        if (semestar == 'zimski' && zimski.includes(danas.getMonth())) istiSemestar = true;
        if (semestar == 'ljetni' && ljetni.includes(danas.getMonth())) istiSemestar = true;
        return (istiSemestar && dan === danUSedmici);
    } else {
        let datum = termin['datum'];
        let danas = new Date();
        var noviDatum = datum.split(".");
        return noviDatum[0] == danas.getDate() && noviDatum[1] == (danas.getMonth() + 1) && noviDatum[2] == danas.getFullYear();
    }
}
function RedovnoZauzece(dan, semestar, pocetak, kraj, naziv, predavac) {
    return {
        dan,
        semestar,
        pocetak,
        kraj,
        naziv,
        predavac
    }
}
function VanrednoZauzece(datum, pocetak, kraj, naziv, predavac) {
    return {
        datum,
        pocetak,
        kraj,
        naziv,
        predavac
    }
}

module.exports = app;