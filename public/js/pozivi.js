let Pozivi = (function () {
  function ucitajPodatkeZaKalendar(callback) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {// Anonimna funkcija
      if (ajax.readyState == 4 && ajax.status == 200) {
        let sadrzaj = ajax.responseText;
        sadrzaj = JSON.parse(sadrzaj);
        let periodicna = sadrzaj["periodicna"];
        let vanredna = sadrzaj["vanredna"];
        callback(periodicna, vanredna);
      }
    }
    ajax.open("GET", "/zauzeca", true);
    ajax.send();
  }

  function posaljiRezervaciju(objekt) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {// Anonimna funkcija
      if (ajax.readyState == 4 && ajax.status == 200) {
        let sadrzaj = ajax.responseText;
        sadrzaj = JSON.parse(sadrzaj);
        if (sadrzaj['imaAlerta']) {
          alert(sadrzaj['alert']);
        }
        let periodicna = sadrzaj["periodicna"];
        let vanredna = sadrzaj["vanredna"];
        Pozivi.ucitajPodatkeZaKalendar((periodicna, vanredna) => {
          Kalendar.ucitajPodatke(periodicna, vanredna);
          let drop = document.getElementById("sala")
          let sala = drop.options[drop.selectedIndex].value;
          let pocetak = document.getElementById("pocetak").value;
          let kraj = document.getElementById("kraj").value;
          let aktivniMjesec = naziviMjeseci.indexOf(document.getElementById("kalendar").children[0].innerHTML);
          Kalendar.iscrtajKalendar(document.getElementById("kalendar"), aktivniMjesec);
          Kalendar.obojiZauzeca(document.getElementById("kalendar"), aktivniMjesec, sala, pocetak, kraj);
        });
      }
    }
    ajax.open("POST", "/", true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(objekt);
  }

  function ucitajSlike(callback) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {// Anonimna funkcija
      if (ajax.readyState == 4 && ajax.status == 200) {
        let sadrzaj = ajax.responseText;
        sadrzaj = JSON.parse(sadrzaj);
        let src1 = sadrzaj["src1"];
        let src2 = sadrzaj["src2"];
        let src3 = sadrzaj["src3"];
        callback(src1, src2, src3, sadrzaj['jeLiKraj']);
      }
    }
    let objekt = { ucitaneSlike: ucitane };
    ajax.open("POST", "/srcSlike", true);
    ajax.setRequestHeader("Content-Type", "application/json");
    ajax.send(JSON.stringify(objekt));
  }

  function ucitajOsoblje(dropdown) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {// Anonimna funkcija
      if (ajax.readyState == 4 && ajax.status == 200) {
        let sadrzaj = ajax.responseText;
        sadrzaj = JSON.parse(sadrzaj);
        let osobe = sadrzaj['imena'];
        for (let i = 0; i < osobe.length; i++) { dropdown.options.add(new Option(osobe[i], osobe[i])); }
      }
    }
    ajax.open("GET", "/osoblje", true);
    ajax.send();
  }

  function ucitajSale(dropdown) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {// Anonimna funkcija
      if (ajax.readyState == 4 && ajax.status == 200) {
        let sadrzaj = ajax.responseText;
        sadrzaj = JSON.parse(sadrzaj);
        let sale = sadrzaj['nazivi'];
        for (let i = 0; i < sale.length; i++) { dropdown.options.add(new Option(sale[i], sale[i])); }
      }
    }
    ajax.open("GET", "/sale", true);
    ajax.send();
  }

  function prikaziOsoblje(lista) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {// Anonimna funkcija
      if (ajax.readyState == 4 && ajax.status == 200) {
        let sadrzaj = ajax.responseText;
        sadrzaj = JSON.parse(sadrzaj);
        let osobe = sadrzaj['osobe'];
        console.log(osobe);
        lista.innerHTML = "";
        for (let i = 0; i < osobe.length; i++) {
          var li = document.createElement('li');
          li.innerHTML = osobe[i];
          lista.appendChild(li);
        }
      }
    }
    ajax.open("GET", "/lokacije", true);
    ajax.send();
  }
  return {
    ucitajPodatkeZaKalendar,
    posaljiRezervaciju,
    ucitajSlike,
    ucitajOsoblje,
    ucitajSale,
    prikaziOsoblje
  }
}());