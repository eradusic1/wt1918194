// pomocne klase koje vracaju objekat zauzeca, imaju funkciju konstruktora s parametrima
function RedovnoZauzece(dan, semestar, pocetak, kraj, naziv, predavac) {
    return {
        dan,
        semestar,
        pocetak,
        kraj,
        naziv,
        predavac
    }
}
function VanrednoZauzece(datum, pocetak, kraj, naziv, predavac) {
    // var noviDatum = datum1.split(".");
    // noviDatum.reverse();
    // noviDatum.join("-");
    return {
        datum,
        pocetak,
        kraj,
        naziv,
        predavac
    }
}
function dajDatum(datum1) {
    var noviDatum = datum1.split(".");
    noviDatum.reverse();
    noviDatum.join("-");
    return new Date(noviDatum);
}
// modul Kalendar
let Kalendar = (function () {
    let periodicna = [];
    let vanredna = [];
    let naziviMjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
    let zimski = [9, 10, 11, 0];
    let ljetni = [1, 2, 3, 4, 5];

    function obojiZauzecaImpl(kalendarRef, mjesec, sala, pocetak, kraj) {
        // vratiti sve na slobodne
        for (i = 0; i < 31; i++) {
            kalendarRef.getElementsByClassName("dan")[i].children[1].className = "slobodna";
        }
        // pronalazak dana koji su zauzeti
        var periodicniDani = [];
        for (i = 0; i < this.periodicna.length; i++) {
            if (odgovarajuciSemestar(mjesec, this.periodicna[i])) {
                if (this.periodicna[i].naziv === sala && terminZauzet(this.periodicna[i], pocetak, kraj)) periodicniDani.push(this.periodicna[i].dan + 1);
            }
        }
        var vanredniDani = [];
        for (i = 0; i < this.vanredna.length; i++) {
            var godinaZauzeca = dajDatum(this.vanredna[i].datum).getFullYear()
            var mjesecZauzeca = dajDatum(this.vanredna[i].datum).getMonth();
            if (mjesecZauzeca === mjesec && this.vanredna[i].naziv === sala && terminZauzet(this.vanredna[i], pocetak, kraj) && godinaZauzeca==new Date().getFullYear()) {
                vanredniDani.push(dajDatum(this.vanredna[i].datum).getDate());
            }
        }

        //mijenjanje klase u zauzeta
        var dani = kalendarRef.getElementsByClassName("dan");
        let offset = kalendarRef.getElementsByClassName("dan")[0].style.gridColumnStart - 1;
        for (i = 0; i < 31; i++) {
            if (vanredniDani.includes(i + 1)) dani[i].children[1].className = "zauzeta";
            let index = (i + 1 + offset) % 7;
            if (index === 0) index = 7;
            if (periodicniDani.includes(index)) dani[i].children[1].className = "zauzeta";
        }
    }

    function ucitajPodatkeImpl(periodicna, vanredna) {
        this.periodicna = periodicna;
        this.vanredna = vanredna;
    }

    function iscrtajKalendarImpl(kalendarRef, mjesec) {
        let trenutnaGodina = new Date().getFullYear();
        kalendarRef.getElementsByClassName("nazivMjeseca")[0].innerHTML = naziviMjeseci[mjesec];
        let prviDan = new Date(trenutnaGodina, mjesec, 1).getDay();
        if (prviDan == 0) prviDan = 7;

        let brojDana = new Date(trenutnaGodina, mjesec, 0).getDate();

        //pomjeranje prvog dana na pravo mjesto
        kalendarRef.getElementsByClassName("dan")[0].style.gridColumnStart = "" + prviDan;

        //svi elementi slobodni
        for (i = 0; i < 31; i++) {
            kalendarRef.getElementsByClassName("dan")[i].children[1].className = "slobodna";
        }

        //redanje tacnog broja elemenata
        for (i = 0; i < 31; i++) {
            if (i >= new Date(trenutnaGodina, mjesec + 1, 0).getDate()) kalendarRef.getElementsByClassName("dan")[i].style.display = "none";
            else kalendarRef.getElementsByClassName("dan")[i].style.display = "inline-block";
        }
    }

    function terminZauzet(zauzece, pocetak, kraj) {
        if (pocetak == kraj) return false;
        if (pocetak > kraj) return false;

        let pocetakSala = new Date('2/2/2000 ' + zauzece.pocetak);
        let krajSala = new Date('2/2/2000 ' + zauzece.kraj);
        let pocetakOgranicenje = new Date('2/2/2000 ' + pocetak);
        let krajOgranicenje = new Date('2/2/2000 ' + kraj);

        if (pocetakSala >= pocetakOgranicenje && pocetakSala < krajOgranicenje) return true;
        if (krajSala > pocetakOgranicenje && krajSala <= krajOgranicenje) return true;
        if (pocetakOgranicenje >= pocetakSala && pocetakOgranicenje < krajSala) return true;
        if (krajOgranicenje > pocetakSala && krajOgranicenje <= krajSala) return true;

        return false;
    }

    function odgovarajuciSemestar(mjesec, rezervacija) {
        if (rezervacija.semestar === "zimski" && zimski.includes(mjesec)) {
            return true;
        }
        else if (rezervacija.semestar === "ljetni" && ljetni.includes(mjesec)) {
            return true;
        } else {
            return false;
        }
    }

    return {
        obojiZauzeca: obojiZauzecaImpl,
        ucitajPodatke: ucitajPodatkeImpl,
        iscrtajKalendar: iscrtajKalendarImpl
    }

}());