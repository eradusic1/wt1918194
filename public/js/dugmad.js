function idiNazad(){
    let naziviMjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
    let mjesec = document.getElementsByClassName("nazivMjeseca")[0].innerHTML;
    let redni = naziviMjeseci.indexOf(mjesec);
    if(redni>0) {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"),redni-1);
        pozoviObojiKalendar(redni-1);
    }
    else 
    document.getElementById("dugmeNazad").disabled = true;
    if(redni==10) document.getElementById("dugmeNaprijed").disabled = false;
}

function idiNaprijed(){
    let naziviMjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];
    let mjesec = document.getElementsByClassName("nazivMjeseca")[0].innerHTML;
    let redni = naziviMjeseci.indexOf(mjesec);
    if(redni<11) {
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"),redni+1);
        pozoviObojiKalendar(redni+1);
    }
    else 
    document.getElementById("dugmeNaprijed").disabled = true;
    if(redni==1) document.getElementById("dugmeNazad").disabled = false;
}

function pozoviObojiKalendar(mjesec){
    let drop = document.getElementById("sala")
    let sala = drop.options[drop.selectedIndex].value;
    let pocetak = document.getElementById("pocetak").value;
    let kraj = document. getElementById("kraj").value;

    Kalendar.obojiZauzeca(document.getElementById("kalendar"), mjesec, sala, pocetak, kraj);    
}