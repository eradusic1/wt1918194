document.getElementById("forma").addEventListener("change", formaListener);
let naziviMjeseci = ["Januar", "Februar", "Mart", "April", "Maj", "Juni", "Juli", "August", "Septembar", "Oktobar", "Novembar", "Decembar"];

function formaListener(){
    let drop = document.getElementById("sala")
    let sala = drop.options[drop.selectedIndex].value;
    let pocetak = document.getElementById("pocetak").value;
    let kraj = document. getElementById("kraj").value;
    let aktivniMjesec = naziviMjeseci.indexOf(document.getElementById("kalendar").children[0].innerHTML);

    Kalendar.obojiZauzeca(document.getElementById("kalendar"), aktivniMjesec, sala, pocetak, kraj);    
}