let ucitane = [];
let gotovoUcitavanje = false;
let htmlovi = document.getElementsByClassName('img');
let folder = '../slike/';
window.onload = function () {
    Pozivi.ucitajSlike((a, b, c, kraj) => {
        if (a != '') htmlovi[0].src = folder + a;
        if (b != '') htmlovi[1].src = folder + b;
        if (c != '') htmlovi[2].src = folder + c;
        if (a === '') { htmlovi[0].style.display = 'none'; }
        if (b === '') { htmlovi[1].style.display = 'none'; }
        if (c === '') { htmlovi[2].style.display = 'none'; }
        ucitane.push(a);
        ucitane.push(b);
        ucitane.push(c);
        gotovoUcitavanje = kraj;
        document.getElementById("dugmeNazad").disabled = true;
        if (gotovoUcitavanje) document.getElementById("dugmeNaprijed").disabled = true;
    });
}

function ucitajPrethodneSlike() {
    if (document.getElementById("dugmeNaprijed").disabled == true) document.getElementById("dugmeNaprijed").disabled = false;
    //if (document.getElementById("dugmeNazad").disabled == true) document.getElementById("dugmeNazad").disabled = false;
    let brojAktivnih = 0;
    for (let i = 0; i < 3; i++) {
        if (htmlovi[i].style.display != 'none') brojAktivnih++;
    }
    let imePrve = htmlovi[0].src.split('/')[4];
    let indexZadnje = ucitane.indexOf(imePrve) - 1;
    htmlovi[0].src = folder + ucitane[indexZadnje - 2];
    htmlovi[1].src = folder + ucitane[indexZadnje - 1];
    htmlovi[2].src = folder + ucitane[indexZadnje];
    if (indexZadnje - 2 == 0) { document.getElementById("dugmeNazad").disabled = true; }
    if (brojAktivnih != 3) {
        htmlovi[0].style.display = 'block'; htmlovi[1].style.display = 'block'; htmlovi[2].style.display = 'block';
    }
}

function ucitajNaredneSlike() {
    if (document.getElementById("dugmeNazad").disabled == true) document.getElementById("dugmeNazad").disabled = false;
    if (!gotovoUcitavanje) {
        Pozivi.ucitajSlike((a, b, c, kraj) => {
            if (a != '') htmlovi[0].src = folder + a;
            if (b != '') htmlovi[1].src = folder + b;
            if (c != '') htmlovi[2].src = folder + c;
            if (a === '') { htmlovi[0].style.display = 'none'; }
            if (b === '') { htmlovi[1].style.display = 'none'; }
            if (c === '') { htmlovi[2].style.display = 'none'; }
            if (a != '') ucitane.push(a);
            if (b != '') ucitane.push(b);
            if (c != '') ucitane.push(c);
            gotovoUcitavanje = kraj;
            if (kraj) {
                document.getElementById("dugmeNaprijed").disabled = true;
            }
        });
    } else {
        let brojAktivnih = 0;
        for (let i = 0; i < 3; i++) {
            if (htmlovi[i].style.display != 'none') brojAktivnih++;
        }
        let imeZadnje = htmlovi[brojAktivnih - 1].src.split('/')[4];
        let indexZadnje = ucitane.indexOf(imeZadnje) + 1;
        if (indexZadnje < ucitane.length) htmlovi[0].src = folder + ucitane[indexZadnje];
        else {
            htmlovi[0].style.display = 'none';
        }
        if (indexZadnje + 1 < ucitane.length) htmlovi[1].src = folder + ucitane[indexZadnje + 1];
        else {
            htmlovi[1].style.display = 'none';
        }
        if (indexZadnje + 2 < ucitane.length) htmlovi[2].src = folder + ucitane[indexZadnje + 2];
        else {
            htmlovi[2].style.display = 'none';
        }
        brojAktivnih = 0;
        for (let i = 0; i < 3; i++) {
            if (htmlovi[i].style.display != 'none') brojAktivnih++;
        }
        imeZadnje = htmlovi[brojAktivnih - 1].src.split('/')[4];
        indexZadnje = ucitane.indexOf(imeZadnje) + 1;
        if (indexZadnje >= ucitane.length) document.getElementById("dugmeNaprijed").disabled = true;
    }
}
