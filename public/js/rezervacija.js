let zimski = [9, 10, 11, 0];
let ljetni = [1, 2, 3, 4, 5];
let zabranjeniMjeseci = [6, 7, 8];
window.onload = function () {
    Pozivi.ucitajPodatkeZaKalendar((periodicna, vanredna) => {
        Kalendar.ucitajPodatke(periodicna, vanredna);
        Kalendar.iscrtajKalendar(document.getElementById("kalendar"), new this.Date().getMonth());
        Pozivi.ucitajOsoblje(document.getElementById("dropOsobe"));
        Pozivi.ucitajSale(document.getElementById("sala"));
    });
}

let dani = document.getElementsByClassName("dan");
for (let i = 0; i < dani.length; i++) {
    dani[i].addEventListener("click", () => {
        if (dani[i].children[1].className == "zauzeta") return;
        let pocetak = document.getElementById("pocetak").value;
        let kraj = document.getElementById("kraj").value;
        if (pocetak.trim() && kraj.trim()) {
            var potvrda = confirm("Da li želite rezervisati ovaj termin?");
            if (potvrda) {
                let dan = i + 1;
                let mjesec = naziviMjeseci.indexOf(document.getElementsByClassName("nazivMjeseca")[0].innerHTML);
                let datum = new Date(new Date().getFullYear() + '-' + (mjesec + 1) + '-' + dan);
                let datumVanredni = dan + '.' + (mjesec + 1) + '.' + new Date().getFullYear();
                let danUSedmici = datum.getDay();
                if (danUSedmici == 0) danUSedmici = 7;
                danUSedmici--;
                let drop = document.getElementById("sala")
                let sala = drop.options[drop.selectedIndex].value;
                let drop2=document.getElementById("dropOsobe");
                let predavac=drop2.options[drop2.selectedIndex].value;
                let oznacenCheckbox = document.getElementById("checkbox").checked;
                let ispravnaForma = provjeriFormu(pocetak, kraj);
                let semestar = "";
                if (ljetni.includes(mjesec)) semestar = "ljetni";
                else if (zimski.includes(mjesec)) semestar = "zimski";
                if (ispravnaForma) {
                    if (oznacenCheckbox == true) {
                        if (zabranjeniMjeseci.includes(mjesec)) {
                            alert("U ovom mjesecu ne možete izvršiti periodičnu rezervaciju!");
                            return;
                        }
                        let redovnoZauzece = RedovnoZauzece(danUSedmici, semestar, pocetak, kraj, sala, predavac);
                        let objekat1 = '{"zauzece":' + JSON.stringify(redovnoZauzece) + ', "jeLiPeriodicno":true,"datum":"' + dan + '/' + (mjesec + 1) + '/' + new Date().getFullYear() + '"}';
                        Pozivi.posaljiRezervaciju(objekat1);
                    }
                    else {
                        let vanrednoZauzece = VanrednoZauzece(datumVanredni, pocetak, kraj, sala, predavac);
                        let objekat2 = '{"zauzece":' + JSON.stringify(vanrednoZauzece) + ', "jeLiPeriodicno":false,"datum":"' + dan + '/' + (mjesec + 1) + '/' + new Date().getFullYear() + '"}';
                        Pozivi.posaljiRezervaciju(objekat2);
                    }
                } else {
                    alert("Vremena na formi nisu ispravnog formata.");
                }
            }
            else return;
        }
    });
}

function provjeriFormu(a, b) {
    let time1 = new Date('2/2/2000 ' + a);
    let time2 = new Date('2/2/2000 ' + b);
    if (time2 <= time1) return false;
    return true;
}