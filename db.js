const Sequelize = require("sequelize");
const sequelize = new Sequelize("DBWT19", "root", "root", { host: "127.0.0.1", port: 3308, dialect: "mysql", logging: false });
const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

// modeli
db.osoblje = sequelize.import(__dirname + '/osoblje.js');
db.rezervacija = sequelize.import(__dirname + '/rezervacija.js');
db.sala = sequelize.import(__dirname + '/sala.js');
db.termin = sequelize.import(__dirname + '/termin.js');

// relacije
// Osoblje - jedan na više - Rezervacija
db.osoblje.hasMany(db.rezervacija, { foreignKey: 'osoba' });
// Rezervacija - jedan na jedan - Termin
db.termin.hasOne(db.rezervacija, { foreignKey: 'termin' });
// Rezervacija - više na jedan - Sala
db.sala.hasMany(db.rezervacija, { foreignKey: 'sala' });
// Sala - jedan na jedan - Osoblje
db.osoblje.hasOne(db.sala, { foreignKey: 'zaduzenaOsoba' });

module.exports=db;